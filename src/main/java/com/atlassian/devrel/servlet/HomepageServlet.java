package com.atlassian.devrel.servlet;

import com.atlassian.devrel.checks.PluginChecks;
import com.atlassian.devrel.plugin.PlatformComponents;
import com.atlassian.plugin.util.VersionStringComparator;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.Maps;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.Map;

/**
 * Serves the developer toolbox home page.
 */
public class HomepageServlet extends RequiresAdminServlet {

    private static final String HOMEPAGE_TEMPLATE = "/templates/homepage.vm";

    private static final String I18N_AVAILABLE_KEY = "i18nAvailable";
    private static final String I18N_STATE_KEY = "i18nActive";
    private static final String SDK_VERSION_KEY = "sdkVersion";
    private static final String ACTIVEOBJECTS_AVAILABLE_KEY = "aoAvailable";
    private static final String PLUGIN_DATA_EDITOR_AVAILABLE_KEY = "pdeAvailable";
    private static final String QUICKRELOAD_AVAILABLE_KEY = "qrAvailable";
    private static final String SYSTEM_INFO_LINK_KEY = "systemInfoLink";
    private static final String PLATFORM_COMPONENTS_KEY = "platformComponents";
    private static final String JAVADOC_KEY = "javadocUrl";
    private static final String REST_API_KEY = "restApisUrl";
    private static final String APP_DOC_KEY = "appDevUrl";
    private static final String DISPLAY_NAME = "displayName";
    private static final String VERSION_KEY = "version";
    private static final String BASEURL_KEY = "baseUrl";
    private static final String PRODUCT_KEY = "product";
    private static final String DOWNLOAD_KEY = "downloadUrl";

    private static final String DOWNLOAD_TEMPLATE = "http://www.atlassian.com/software/%s/download?utm_source=developer-toolbox-plugin&utm_medium=homepage-link&utm_campaign=developer-toolbox";

    private final ApplicationProperties applicationProperties;
    private final PlatformComponents platformComponents;
    private final PluginChecks checks;

    public HomepageServlet(UserManager userManager, TemplateRenderer renderer, LoginUriProvider loginUriProvider,
                           ApplicationProperties applicationProperties, PlatformComponents platformComponents,
                           PluginChecks checks) {
        super(userManager, renderer, loginUriProvider);
        this.applicationProperties = applicationProperties;
        this.platformComponents = platformComponents;
        this.checks = checks;
    }

    @Override
    public Map<String, Object> getContext(HttpServletRequest req) {
        Map<String, Object> context = Maps.newHashMap();

        Application application = Application.getApplication(applicationProperties);

        boolean available = isI18nTranslationAvailable(application);
        context.put(I18N_AVAILABLE_KEY, available);
        if (available) {
            context.put(I18N_STATE_KEY, getI18nState(req, application));
        }

        context.put(SDK_VERSION_KEY, getSdkVersion());
        context.put(QUICKRELOAD_AVAILABLE_KEY, checks.isQuickReloadAvailable());
        context.put(ACTIVEOBJECTS_AVAILABLE_KEY, checks.isActiveObjectsAvailable());
        context.put(PLUGIN_DATA_EDITOR_AVAILABLE_KEY, checks.isPluginDataEditorAvailable());
        context.put(SYSTEM_INFO_LINK_KEY, getSystemInfoLink(application));
        context.put(PLATFORM_COMPONENTS_KEY, platformComponents.getPlatformComponents());
        context.put(JAVADOC_KEY, getJavadocLink(application));
        context.put(REST_API_KEY, getRestApisLink(application));
        context.put(APP_DOC_KEY, getAppDevDocLink(application));
        context.put(DISPLAY_NAME, getDisplayName(application));
        context.put(VERSION_KEY, applicationProperties.getVersion());
        context.put(BASEURL_KEY, getBaseUrl());
        context.put(PRODUCT_KEY, application.slug());
        context.put(DOWNLOAD_KEY, getDownloadUrl(application));

        return context;
    }

    private String getDownloadUrl(Application application) {
        if (application == null) {
            return String.format(DOWNLOAD_TEMPLATE, applicationProperties.getDisplayName().toLowerCase(Locale.ENGLISH));
        } else {
            if (application == Application.STASH) {
                application = Application.BITBUCKET;
            }
            return String.format(DOWNLOAD_TEMPLATE, application.slug());
        }
    }

    @Override
    public String getTemplatePath() {
        return HOMEPAGE_TEMPLATE;
    }

    private boolean isI18nTranslationAvailable(Application application) {
        return application == Application.JIRA || application == Application.CONFLUENCE;
    }

    private boolean getI18nState(HttpServletRequest req, Application application) {
        HttpSession session = req.getSession(false);
        if (application == Application.JIRA) {
            return session.getAttribute("com.atlassian.jira.util.i18n.I18nTranslationModeSwitch") != null;
        } else if (application == Application.CONFLUENCE) {
            Object o = session.getAttribute("confluence.i18n.mode");
            if (o != null) {
                return o.getClass().getSimpleName().equals("LightningTranslationMode");
            }
        }

        return false;
    }

    private String getAppDevDocLink(Application application) {
        if (isCloud()) {
            switch (application) {
                case CONFLUENCE:
                    return "https://developer.atlassian.com/cloud/confluence/getting-started/";
                case JIRA:
                    return "https://developer.atlassian.com/cloud/jira/platform/getting-started/";
                case BITBUCKET:
                    return "https://developer.atlassian.com/cloud/bitbucket/getting-started/";
                default:
                    return "https://developer.atlassian.com/cloud/";
            }
        }
        switch (application) {
            case CONFLUENCE:
                return "https://developer.atlassian.com/server/confluence/confluence-plugin-guide/";
            case JIRA:
                return "https://developer.atlassian.com/server/jira/platform/getting-started/";
            case STASH:
            case BITBUCKET:
                return "https://developer.atlassian.com/server/bitbucket/how-tos/beginner-guide-to-bitbucket-server-plugin-development/";
            case BAMBOO:
                return "https://developer.atlassian.com/server/bamboo/";
            case CRUCIBLE:
            case FISHEYE:
                return "https://developer.atlassian.com/server/fisheye-crucible/";
            case CROWD:
                return "https://developer.atlassian.com/server/crowd/";
            default:
                return "https://developer.atlassian.com/server/";
        }
    }

    private String getRestApisLink(Application application) {
        if (isCloud()) {
            switch (application) {
                case CONFLUENCE:
                    return "https://developer.atlassian.com/cloud/confluence/rest/";
                case JIRA:
                    return "https://developer.atlassian.com/cloud/jira/platform/rest/";
                case BITBUCKET:
                    return "https://developer.atlassian.com/bitbucket/api/2/reference";
                default:
                    return null;
            }
        }
        switch (application) {
            case CONFLUENCE:
                return "https://docs.atlassian.com/ConfluenceServer/rest/latest/";
            case JIRA:
                return "https://docs.atlassian.com/software/jira/docs/api/REST/latest/";
            case STASH:
            case BITBUCKET:
                return "https://developer.atlassian.com/server/bitbucket/reference/rest-api/";
            case BAMBOO:
                return "https://docs.atlassian.com/bamboo/REST/latest/";
            case CRUCIBLE:
            case FISHEYE:
                return "https://developer.atlassian.com/server/fisheye-crucible/rest-api-guide/";
            case CROWD:
                return "https://docs.atlassian.com/atlassian-crowd/latest/REST/";
            default:
                return null;
        }
    }

    private String getJavadocLink(Application application) {
        if (isCloud()) {
            return null;
        }
        switch (application) {
            case CONFLUENCE:
                return "https://developer.atlassian.com/server/confluence/java-api-reference/";
            case JIRA:
                return "https://developer.atlassian.com/server/jira/platform/java-apis/";
            case STASH:
            case BITBUCKET:
                return "https://developer.atlassian.com/server/bitbucket/reference/java-api/";
            case BAMBOO:
                return "https://developer.atlassian.com/server/bamboo/java-api-reference/";
            case CRUCIBLE:
            case FISHEYE:
                return "https://developer.atlassian.com/server/fisheye-crucible/java-api-reference/";
            case CROWD:
                return "http://docs.atlassian.com/atlassian-crowd/latest/";
            default:
                return null;
        }
    }

    private String getSdkVersion() {
        return System.getProperty("atlassian.sdk.version", "3.7 or earlier");
    }

    private String getSystemInfoLink(Application application) {
        if (application == Application.JIRA) {
            return getBaseUrl() + "/secure/admin/ViewSystemInfo.jspa";
        } else if (application == Application.CONFLUENCE || application == Application.BAMBOO) {
            return getBaseUrl() + "/admin/systeminfo.action";
        } else if (application == Application.FISHEYE || application == Application.CRUCIBLE) {
            return getBaseUrl() + "/admin/sysinfo.do";
        } else {
            // can't get right now
            return null;
        }
    }

    private String getBaseUrl() {
        return applicationProperties.getBaseUrl(UrlMode.RELATIVE);
    }

    private String getDisplayName(Application application) {
        String name = applicationProperties.getDisplayName();

        switch (application) {
            case FISHEYE:
            case CRUCIBLE:
            case BAMBOO:
            case CROWD:
                // these are Server- or DC-only products.
                return name;
            default:
                // these might be either Cloud or Server. Check the version to determine; Cloud product versions are > 1000.
                return name + (isCloud() ? " Cloud" : " Server");
        }
    }

    private boolean isCloud() {
        return new VersionStringComparator().compare(applicationProperties.getVersion(), "1000") > 0;
    }

    private enum Application {
        JIRA, CONFLUENCE, CROWD, FISHEYE, CRUCIBLE, BAMBOO, STASH, BITBUCKET, UNKNOWN;

        private static Application getApplication(ApplicationProperties properties) {
            String displayName = properties.getDisplayName();
            for (Application application : values()) {
                if (application.name().equalsIgnoreCase(displayName)) {
                    return application;
                }
            }
            return UNKNOWN;
        }

        String slug() {
            return this.name().toLowerCase(Locale.ENGLISH);
        }
    }
}
