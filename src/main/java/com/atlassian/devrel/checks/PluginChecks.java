package com.atlassian.devrel.checks;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;

import javax.annotation.Nonnull;
import java.util.Optional;
import java.util.function.Function;

public class PluginChecks {
    private static final String QR_PLUGIN_KEY = "com.atlassian.labs.plugins.quickreload.reloader";
    private static final String DEVTOOLBOX_PLUGIN_KEY = "com.atlassian.devrel.developer-toolbox-plugin";

    private PluginAccessor pluginAccessor;

    public PluginChecks(PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    public boolean isActiveObjectsAvailable() {
        return plugin("com.atlassian.activeobjects.activeobjects-plugin").isPresent();
    }

    public boolean isPluginDataEditorAvailable() {
        return plugin("com.atlassian.plugins.plugin-data-editor").isPresent();
    }

    public boolean isQuickReloadAvailable() {
        return plugin(QR_PLUGIN_KEY).isPresent();
    }

    public String getQuickReloadVersion() {
        return pluginVersion(QR_PLUGIN_KEY);
    }

    public String getDevtoolboxVersion() {
        return pluginVersion(DEVTOOLBOX_PLUGIN_KEY);
    }

    private Optional<Plugin> plugin(@Nonnull final String pluginKey) {
        return Optional.ofNullable(pluginAccessor.getEnabledPlugin(pluginKey));
    }

    private String pluginVersion(@Nonnull final String pluginKey) {
        return plugin(pluginKey)
                .flatMap(new Function<Plugin, Optional<String>>() {
                    @Override
                    public Optional<String> apply(Plugin p) {
                        return Optional.of(p.getPluginInformation().getVersion());
                    }
                })
                .orElse("disabled");
    }
}
