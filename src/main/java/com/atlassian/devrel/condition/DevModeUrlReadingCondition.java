package com.atlassian.devrel.condition;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.condition.UrlReadingCondition;
import com.atlassian.plugin.webresource.url.UrlBuilder;

import java.util.Map;

import static java.lang.Boolean.getBoolean;
import static java.lang.Boolean.parseBoolean;

public class DevModeUrlReadingCondition implements UrlReadingCondition
{
    private static final String DEVTOOLBAR_QUERY_PARAM = "devtoolbar";

    public static boolean isDevToolbarEnabled() {
        return getBoolean("atlassian.dev.mode") || getBoolean("devtoolbar.enable");
    }

    @Override
    public void addToUrl(final UrlBuilder urlBuilder)
    {
        urlBuilder.addToQueryString(DEVTOOLBAR_QUERY_PARAM, String.valueOf(isDevToolbarEnabled()));
    }

    @Override
    public boolean shouldDisplay(final QueryParams params)
    {
        return parseBoolean(params.get(DEVTOOLBAR_QUERY_PARAM));
    }

    @Override
    public void init(final Map<String, String> params) throws PluginParseException {}

}
