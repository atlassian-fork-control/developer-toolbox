AJS.toInit(function () {
    var productId = AJS.$('#current-product-version').data('product');
    if (productId) {
        AJS.$.ajax({
            url: 'https://my.atlassian.com/download/feeds/current/' + productId + '.json',
            dataType: 'jsonp',
            jsonpCallback: 'downloads',
            success: function (data) {
                var version = data[0].version;
                AJS.$("#latest-product-version").html('(' + version + ')');
            }
        });
    }
});
